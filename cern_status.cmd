REM set vlc=f:\VLC\bin\vlc-2.2.8\vlc.exe
REM set vlc=f:\VLC\bin\vlc-2.1.5-win32\vlc-2.1.5\vlc.exe
set vlc=f:\VLC\bin\vlc-2.1.5-win64\vlc-2.1.5\vlc.exe

REM set sout=
REM set sout=--sout '#duplicate{dst=display,dst=rtp{mux=ts,dst=192.168.1.12}}'
REM set sout=--rtsp-host 0.0.0.0 --rtsp-port 554 -sout '#duplicate{dst=display,dst=rtp{mux=ts,dst=0.0.0.0:554}}'

start %vlc% --input-title-format="CERN Machine Status Pages" --sub-source=mosaic --mosaic-width=1600 --mosaic-height=1200 --mosaic-rows=2 --mosaic-cols=2 --mosaic-order="1,2,3,4" --mosaic-keep-picture --vlm-conf=cern_status.vlm --image-duration=-1 --image-fps=25 bg_1600x1200.png %sout%