# CERN status

The mosaic view of four CERN machine status pages (PSB, CPS, SPS and LHC).
The VLC 2.x is needed, as the UDP multicast reception is not working in version 3 and 4 right now on our streams.
More info about streams [here](https://wikis.cern.ch/display/COIN/Machine+Status+Pages).